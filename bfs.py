import collections
import queue
class Graph:
    def __init__(self, vertices) -> None:
        self.V =vertices
        self.adjList = collections.defaultdict(list)
        
    #add edge
    def add_edge(self, src, dest):
        self.adjList[src].append(dest)
        self.adjList[dest].append(src)
      
    def bfs(self, src):
        queue = collections.deque()
        queue.append(src)
        visited  =set()
        while queue:
            node= queue.pop()
            visited.add(node)

            for nbr in self.adjList[node]:
                if nbr not in visited:
                    queue.append(nbr)
        print(visited)            
if __name__ == '__main__':
        g = Graph(5)
        g.add_edge(0,1)               
        g.add_edge(0,2)               
        g.add_edge(1,3)               
        g.add_edge(1,4)               
        g.add_edge(2,5)               
        g.add_edge(2,6) 
        g.bfs(0)                            
        print(g.adjList)