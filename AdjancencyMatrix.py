# class Graph(object):
#     def __init__(self, size) -> None:
#         self.adjMatrix =[]
#         for i in range(size):
#             self.adjMatrix.append([0 for i in range(size)])
#             self.size=size
#     #add edge
#     def add_edge(self, v1, v2):         

class Graph:
    def __init__(self, vertices) -> None:
        self.V =vertices
        self.adjMatrix =[[0]*self.V for i in range(self.V)]

    #add edge
    def add_edge(self, src, dest):
        self.adjMatrix[src][dest] =1
        self.adjMatrix[dest][src] =1

if __name__ == '__main__':
        g = Graph(5)
        g.add_edge(0,1)               
        g.add_edge(0,3)               
        g.add_edge(0,2)               
        g.add_edge(1,3)               
        g.add_edge(1,4)               
        g.add_edge(2,3)               
        g.add_edge(3,3)               
        g.add_edge(3,4)               
        print(g.adjMatrix)