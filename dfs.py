from collections import defaultdict

class Graph:
    def __init__(self, vertices) -> None:
        self.V = vertices
        self.adjList = defaultdict(list)
    # Helper function
    def dfsHelper(self, src, visited):
        visited.add(src)
        print(src, end =" ")

        # iterate queue data structure
        for node in self.adjList[src]:
            if node not in visited:
                self.dfsHelper(node, visited)
        return        

    def add_edge(self, src, dest):
        self.adjList[src].append(dest)
        self.adjList[dest].append(src) 

    def dfs(self, src):
        visited =set()  
        self.dfsHelper(src, visited)  


if __name__ == '__main__':
    g=Graph(5)
    g.add_edge(0,1)
    g.add_edge(0,2) 
    g.add_edge(1,3)
    g.add_edge(1,4) 
    g.add_edge(2,5)
    g.add_edge(2,6)   
    g.dfs(0)   




# second Method
from collections import defaultdict

class Graph:
    def __init__(self, vertices) -> None:
        self.V = vertices
        self.adjList = defaultdict(list)      

    def add_edge(self, src, dest):
        self.adjList[src].append(dest)
        self.adjList[dest].append(src) 

    def dfs(self, src, visited=None):
        if visited is None:
            visited =set() 
        visited.add(src)
        # print(src, end=" ")

        for nbr in self.adjList[src]:
            if nbr not in visited:
                self.dfs(nbr, visited)        

if __name__ == '__main__':
    g=Graph(5)
    g.add_edge(0,1)
    g.add_edge(0,2) 
    g.add_edge(1,3)
    g.add_edge(1,4) 
    g.add_edge(2,5)
    g.add_edge(2,6)   
    g.dfs(0)   
    print(g.adjList)

