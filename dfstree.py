from collections import defaultdict
def dfs(src, p):
    parent[src] = p
    print(src, end=" ")
    for nbr in adjlist[src]:
        if nbr != p:
            dfs(nbr, src)

if __name__ == '__main__':
    adjlist =defaultdict(list)
    edges= [[1,2],[1,3],[3,4],[3,5]]

    for src,dest in edges:
        adjlist[src].append(dest)
        adjlist[dest].append(src)  

    N = len(adjlist)
    parent = [-1]*(N+1)

    dfs(1, -1)      