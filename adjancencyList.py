import collections
# dict, defaultdict
class Graph:
    #constructure
    def __init__(self) -> None:
        self.adjList = collections.defaultdict(list)
    # method    
    def addAdj(self, src, dest):
        self.adjList[src].append(dest)
        self.adjList[dest].append(src)
if __name__ == '__main__':
    g = Graph() # creating instance/object
    g.addAdj(0,1)
    g.addAdj(0,3)
    g.addAdj(0,5)  
    g.addAdj(1,2) 
    g.addAdj(1,3)
    g.addAdj(2,3)
    g.addAdj(3,4)
    g.addAdj(4,5)
    print(g.adjList) 
                    
